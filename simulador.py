from math import *
from time import sleep

from graphics import *

Vo=60
t=0
wo=60*3.1415926535/180
y=0
g=11
x=0

ventana=GraphWin("Simulador",600,600)
ventana.setCoords(0,0,400,400)
while(y>=0):
    x=Vo*cos(wo)*t
    y=Vo*sin(wo)*t-(1.0/2)*g*t*t
    miCirculo=Circle(Point(x,y),5)
    miCirculo.setFill('blue')
    miCirculo.draw(ventana)
    print(x,y)
    sleep(0.1)
    t=t+0.1
ventana.getMouse()
